# GameOfThrees API
My submission for the Takeaway.com coding challenge. Completed in one evening.

## NOTE
The brief was outlined in a PDF. I interpreted the document as a guide but not as strict documentation. I following the general goal of the document but made adjustments where I thought appropriate in order to demonstrate key skills in a short amount of time (due to the need to get feeback and progress in the interview quickly because of other potentially pending contracts). I discuss things I would have done differently in production or with more time below but overall I'm very pleased with the results and happy to talk more about them in person.


## Play
The easiest way to play is with the **SwaggerUI** served from the index.

- `make dev`
- `open http://localhost:8080`

**Users are unique PER SESSION** so if you want to play against someone, use a different browser or an incognito window for the second player.

The API is a simple CRUD app. To play a game:

- Fetch your user from `GET /user`
- Create a game at `POST /games` and note the ID
- Make a move with `POST /games/{game-id}/move`
- Open another browser and join the game `POST /games/{game-id}/join`
- In browser2 make a move
- Alternate browsers until one player wins
- Create a new game to continue playing

## About

### Intro
This is a Java8 SpringBoot Gradle application. It uses H2 for storage and JSESSIONID cookies for user identification. Obviously this project is not production ready but it was designed to solve the task in one evening while at the same time demonstrating key skills in testing and development. I would be more than happy to discuss in person how I would approach this for a scalable production app.

I thought about including a VueJS frontend but decided for the time required it wouldn't add value. I can demonstrate my frontend skills with repositories of my own work if necessary. SwaggerUI is used instead as a simple GUI and documentation.

### Structure
I followed a DDD approach when separating concerns into entities, repositories, controllers, services, dtos and config. I use dependency injection to keep components slim and testable. I have added comments throughout the src and tests and within the swagger annotations.


### Testing
I tested the key areas with unit tests and an E2E test that plays a real game. I have written 100% test coverage for many other projects but for sake of time I did not do so here. Hopefully the tests I've written demonstrate my understanding of testing practices.

### What could be improved
This app is not production ready, however it is stable and well tested. For production there would be many changes, including proper DB persistence, real user authentication, scoring, a frontend, circle-ci scripts, high test coverage, docker files, ELB deployment, monitoring, logging etc, and more documentation. Perhaps a messaging queue would also suit this application if many games are occurring simultaneously. I also thought of adding a simple random UI or a bot that calculates the best chance of winning.