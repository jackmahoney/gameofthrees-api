dev:
	./gradlew bootRun

lint:
	./gradlew spotlessCheck

fmt:
	./gradlew spotlessApply

test:
	./gradlew test

report-test:
	firefox build/reports/tests/test/index.html

report-coverage:
	firefox build/reports/coverage/index.html