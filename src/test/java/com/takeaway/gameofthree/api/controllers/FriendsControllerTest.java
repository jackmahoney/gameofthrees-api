package com.takeaway.gameofthree.api.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import com.takeaway.gameofthree.api.entities.User;
import com.takeaway.gameofthree.api.services.DtoService;
import com.takeaway.gameofthree.api.services.UserService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("test")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FriendsControllerTest {

  MockMvc mvc;

  @Mock DtoService dtoService;

  @Mock UserService userService;

  @InjectMocks FriendsController friendsController;

  @Before
  public void setup() {
    mvc = MockMvcBuilders.standaloneSetup(friendsController).build();
  }

  @Test
  public void canGetUser() throws Exception {
    User user = mock(User.class);
    List<User> friends = Arrays.asList(mock(User.class), mock(User.class));
    when(userService.getFriends(user)).thenReturn(friends);
    when(userService.getUserForSession(any())).thenReturn(user);

    mvc.perform(get("/friends")).andExpect(status().isOk());
    verify(dtoService, times(2)).convert(any(User.class));
  }
}
