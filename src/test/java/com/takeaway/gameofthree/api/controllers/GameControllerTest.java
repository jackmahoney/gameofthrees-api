package com.takeaway.gameofthree.api.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Optional;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import com.takeaway.gameofthree.api.dtos.GameCreateDto;
import com.takeaway.gameofthree.api.dtos.GameDto;
import com.takeaway.gameofthree.api.entities.Game;
import com.takeaway.gameofthree.api.entities.User;
import com.takeaway.gameofthree.api.services.DtoService;
import com.takeaway.gameofthree.api.services.GameService;
import com.takeaway.gameofthree.api.services.UserService;
import com.vladmihalcea.hibernate.type.json.internal.JacksonUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("test")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GameControllerTest {

  MockMvc mvc;

  @Mock DtoService dtoService;

  @Mock UserService userService;

  @Mock GameService gameService;

  @InjectMocks GameController gameController;

  @Mock User user;

  @Before
  public void setup() {
    mvc = MockMvcBuilders.standaloneSetup(gameController).build();
    when(userService.getUserForSession(any())).thenReturn(user);
  }

  @Test
  public void canGetGames() throws Exception {
    Game game = mock(Game.class);
    when(gameService.getGamesForUser(user)).thenReturn(Arrays.asList(game));
    when(dtoService.convert(game)).thenReturn(new GameDto());
    mvc.perform(get("/games"))
        .andExpect(status().isOk())
        .andExpect(content().json(JacksonUtil.toString(Arrays.asList(new GameDto()))));
  }

  @Test
  public void canCreateGame() throws Exception {
    Game game = mock(Game.class);
    GameCreateDto dto = GameCreateDto.builder().build();
    when(gameService.createGame(user, Optional.empty())).thenReturn(game);
    when(dtoService.convert(game)).thenReturn(new GameDto());
    mvc.perform(
            post("/games")
                .content(JacksonUtil.toString(dto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andExpect(content().json(JacksonUtil.toString(new GameDto())));
  }
}
