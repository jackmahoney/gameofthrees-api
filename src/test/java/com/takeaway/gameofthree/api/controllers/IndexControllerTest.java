package com.takeaway.gameofthree.api.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("test")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class IndexControllerTest {

  MockMvc mvc;

  @InjectMocks IndexController indexController;

  @Before
  public void setup() {
    mvc = MockMvcBuilders.standaloneSetup(indexController).build();
  }

  @Test
  public void indexRedirects() throws Exception {
    mvc.perform(get("/"))
        .andExpect(status().isFound())
        .andExpect(header().stringValues("Location", "/swagger-ui.html"));
  }
}
