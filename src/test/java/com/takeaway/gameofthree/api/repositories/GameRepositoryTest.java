package com.takeaway.gameofthree.api.repositories;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

import java.util.List;

import com.takeaway.gameofthree.api.entities.Game;
import com.takeaway.gameofthree.api.entities.User;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@DataJpaTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class GameRepositoryTest {

  @Autowired GameRepository gameRepository;

  @Autowired UserRepository userRepository;

  @Autowired TestEntityManager entityManager;

  @Test
  public void canCreateAGame() {
    // start with empty db
    assertThat(gameRepository.count(), equalTo(0L));
    Game game = new Game();
    // save a user
    gameRepository.save(game);
    entityManager.flush();
    assertThat(gameRepository.count(), equalTo(1L));
    // check persistence
    List<Game> games = gameRepository.findAll();
    assertThat("has 1", games.size(), equalTo(1));
    assertThat("UUID is generated", games.get(0).getId(), notNullValue());
  }

  @Test
  public void canAssociatePlayers() {
    // create a user in empty db
    assertThat(userRepository.count(), equalTo(0L));
    User user = userRepository.save(new User());
    userRepository.save(user);
    assertThat("user has UUID", user.getId(), notNullValue());
    entityManager.flush();
    assertThat(userRepository.count(), equalTo(1L));

    // assert game table empty
    assertThat(gameRepository.count(), equalTo(0L));
    Game game = new Game();
    game.setPlayer1(user);
    gameRepository.save(game);
    entityManager.flush();

    // can associate a player with player1
    List<Game> games = gameRepository.findAll();
    assertThat("has 1", games.size(), equalTo(1));
    assertThat("game has correct player", games.get(0).getPlayer1().getId(), equalTo(user.getId()));

    User user2 = userRepository.save(new User());
    userRepository.save(user2);
    assertThat("user 2 has UUID", user2.getId(), notNullValue());
    entityManager.flush();
    assertThat(userRepository.count(), equalTo(2L));

    // can associate a player with player2
    Game game2 = gameRepository.findAll().get(0);
    assertThat(game2.getPlayer2().isPresent(), equalTo(false));
    game2.setPlayer2(user2);
    Game savedGame2 = gameRepository.save(game);

    assertThat(savedGame2.getPlayer2().isPresent(), equalTo(true));
    assertThat(savedGame2.getPlayer2().get().getId(), equalTo(user2.getId()));
  }
}
