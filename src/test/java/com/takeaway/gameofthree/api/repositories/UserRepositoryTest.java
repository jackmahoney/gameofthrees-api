package com.takeaway.gameofthree.api.repositories;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

import java.util.Optional;

import com.takeaway.gameofthree.api.entities.User;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@DataJpaTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class UserRepositoryTest {

  @Autowired UserRepository userRepository;

  @Autowired TestEntityManager entityManager;

  @Test
  public void canCreateAUserWithUniqueSessionId() {
    // start with empty db
    String sessionId = "test-session-id";
    assertThat(userRepository.count(), equalTo(0L));
    User user = new User();
    user.setSessionId(sessionId);
    // save a user
    userRepository.save(user);
    entityManager.flush();
    assertThat(userRepository.count(), equalTo(1L));
    // check persistence
    Optional<User> bySessionId = userRepository.findBySessionId(sessionId);
    assertThat("Fetched User has sessionId we set", bySessionId.isPresent(), equalTo(true));
    assertThat("UUID is generated", bySessionId.get().getId(), notNullValue());
    // test uniqueness of sessionId
    assertThatThrownBy(
            () -> {
              User user2 = new User();
              user2.setSessionId(sessionId);
              userRepository.save(user2);
              entityManager.flush();
            })
        .isInstanceOf(javax.persistence.PersistenceException.class)
        .hasCauseInstanceOf(ConstraintViolationException.class);
  }
}
