package com.takeaway.gameofthree.api.e2e;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import com.takeaway.gameofthree.api.ApiApplication;
import com.takeaway.gameofthree.api.dtos.*;
import com.takeaway.gameofthree.api.enums.MoveAction;
import com.takeaway.gameofthree.api.repositories.UserRepository;
import com.takeaway.gameofthree.api.services.RandomNumberService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/** An end to end test that simulates two players playing the game with fixed starting values */
@RunWith(SpringRunner.class)
@SpringBootTest(
  classes = ApiApplication.class,
  webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@Transactional
@ActiveProfiles(profiles = "test")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GameE2E {

  // we want to make games non-random for testing
  @MockBean RandomNumberService randomNumberService;

  static final String LOCAL_HOST = "http://localhost:";

  final int START = 10;

  @Autowired UserRepository userRepository;

  @LocalServerPort int port;

  String createURL(String uri) {
    return LOCAL_HOST + port + uri;
  }

  TestRestTemplate template = new TestRestTemplate();

  @Autowired EntityManager entityManager;

  // we will simulate two users
  List<String> user1Cookie;
  List<String> user2Cookie;
  UserDto user1;
  UserDto user2;
  GameDto game;

  @Before
  public void before() {
    when(randomNumberService.getRandomIntegerBetween(any(), any())).thenReturn(START);
  }

  private List<String> getCookies(ResponseEntity re) {
    return re.getHeaders().get("Set-Cookie");
  }

  private <T> ResponseEntity<T> getWithCookie(List<String> cookie, String path, Class<T> clazz) {
    HttpHeaders requestHeaders = new HttpHeaders();
    requestHeaders.put(HttpHeaders.COOKIE, cookie);
    return template.exchange(
        createURL(path), HttpMethod.GET, new HttpEntity<>(requestHeaders), clazz);
  }

  private <T> ResponseEntity<T> exchangeWithCookie(
      List<String> cookie, HttpMethod method, String path, Class<T> clazz, Object payload) {
    HttpHeaders requestHeaders = new HttpHeaders();
    requestHeaders.put(HttpHeaders.COOKIE, cookie);
    requestHeaders.setContentType(MediaType.APPLICATION_JSON);
    HttpEntity<Object> httpEntity = new HttpEntity<>(payload, requestHeaders);
    return template.exchange(createURL(path), method, httpEntity, clazz);
  }

  //---- e2e test ---- //

  @Test
  public void canPlayAGameWithAnotherUserStartToFinish() throws IOException {
    createUser1();
    seeEmptyFriendListUser1();
    createUser2();
    seeUser1HasAFriend();
    seeUser2HasAFriend();
    user1CanCreateAGame();
    user1CannotJoinOwnGame();
    user1CanMakeFirstMove();
    user1CanNotMakeAnotherMoveYet();
    user2CanJoinTheGame();
    user2CanMakeAMoveAndWin();
    neitherPlayerCanMakeAnotherMove();
  }

  private void neitherPlayerCanMakeAnotherMove() {
    ResponseEntity<GameDto> response1 =
        exchangeWithCookie(
            user1Cookie,
            HttpMethod.POST,
            "/games/" + game.getId() + "/move",
            GameDto.class,
            getMove(MoveAction.PASS));
    assertThat(response1.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    ResponseEntity<GameDto> response2 =
        exchangeWithCookie(
            user2Cookie,
            HttpMethod.POST,
            "/games/" + game.getId() + "/move",
            GameDto.class,
            getMove(MoveAction.PASS));
    assertThat(response2.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
  }

  private void user2CanMakeAMoveAndWin() {
    ResponseEntity<GameDto> response =
        exchangeWithCookie(
            user2Cookie,
            HttpMethod.POST,
            "/games/" + game.getId() + "/move",
            GameDto.class,
            getMove(MoveAction.ADD_1));
    GameDto gameDto = response.getBody();
    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    assertThat(gameDto.getMoves().size(), equalTo(2));
    MoveDto move1 = response.getBody().getMoves().get(0);
    assertThat(move1.getAction(), equalTo(MoveAction.PASS));
    MoveDto move2 = response.getBody().getMoves().get(1);
    assertThat(move2.getAction(), equalTo(MoveAction.ADD_1));
    assertThat(move2.getInput(), equalTo(move1.getOutput()));
    assertThat(move2.getOutput(), equalTo(1));
    assertThat(gameDto.getWinner().isPresent(), equalTo(true));
    assertThat("user 2 wins the game", gameDto.getWinner().get(), equalTo(user2.getId()));
  }

  private void user2CanJoinTheGame() {
    ResponseEntity<GameDto> response =
        exchangeWithCookie(
            user2Cookie, HttpMethod.POST, "/games/" + game.getId() + "/join", GameDto.class, null);
    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    assertThat(response.getBody().getPlayer2().get(), equalTo(user2.getId()));
  }

  //---- support methods ---- //

  private void user1CanNotMakeAnotherMoveYet() {
    ResponseEntity<GameDto> response =
        exchangeWithCookie(
            user1Cookie,
            HttpMethod.POST,
            "/games/" + game.getId() + "/move",
            GameDto.class,
            getMove(MoveAction.PASS));
    assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
  }

  private void user1CanMakeFirstMove() {
    ResponseEntity<GameDto> response =
        exchangeWithCookie(
            user1Cookie,
            HttpMethod.POST,
            "/games/" + game.getId() + "/move",
            GameDto.class,
            getMove(MoveAction.PASS));
    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    assertThat(response.getBody().getMoves().size(), equalTo(1));
    MoveDto move = response.getBody().getMoves().get(0);
    assertThat(move.getAction(), equalTo(MoveAction.PASS));
    assertThat(move.getInput(), equalTo(START));
    assertThat(move.getOutput(), equalTo(START / 3));
    assertThat(move.getPlayer(), equalTo(user1.getId()));
  }

  private MoveInputDto getMove(MoveAction action) {
    return MoveInputDto.builder().action(action).build();
  }

  private void user1CannotJoinOwnGame() {
    ResponseEntity<GameDto> response =
        exchangeWithCookie(
            user1Cookie, HttpMethod.POST, "/games/" + game.getId() + "/join", GameDto.class, null);
    assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
  }

  private void user1CanCreateAGame() {
    GameCreateDto dto = new GameCreateDto();
    dto.setPlayer2(Optional.empty());
    ResponseEntity<GameDto> response =
        exchangeWithCookie(user1Cookie, HttpMethod.POST, "/games", GameDto.class, dto);
    game = response.getBody();
    assertThat(response.getStatusCode(), equalTo(HttpStatus.CREATED));
    assertThat(game.getCurrentPlayer(), equalTo(user1.getId()));
    assertThat(game.getPlayer1(), equalTo(user1.getId()));
    assertThat(game.getPlayer2(), equalTo(Optional.empty()));
    assertThat(game.getStartingValue(), equalTo(START));
  }

  private void seeUser1HasAFriend() {
    // get user1 friends
    ResponseEntity<UserDto[]> user1Friends =
        getWithCookie(user1Cookie, "/friends", UserDto[].class);
    assertThat(user1Friends.getBody().length, equalTo(1));
    assertThat(user1Friends.getBody()[0].getId(), equalTo(user2.getId()));
  }

  private void seeUser2HasAFriend() {
    // get user1 friends
    ResponseEntity<UserDto[]> user1Friends =
        getWithCookie(user2Cookie, "/friends", UserDto[].class);
    assertThat(user1Friends.getBody().length, equalTo(1));
    assertThat(user1Friends.getBody()[0].getId(), equalTo(user1.getId()));
  }

  private void createUser2() {
    // create user 2
    ResponseEntity<UserDto> response2 = template.getForEntity(createURL("/user"), UserDto.class);
    user2Cookie = getCookies(response2);
    assertThat(user2Cookie.size(), equalTo(1));
    user2 = response2.getBody();
  }

  private void seeEmptyFriendListUser1() {
    // get user1 friends
    ResponseEntity<UserDto[]> user1Friends =
        getWithCookie(user1Cookie, "/friends", UserDto[].class);
    assertThat(user1Friends.getBody().length, equalTo(0));
  }

  private void createUser1() {
    assertThat("start with no user sessions", user1Cookie, nullValue());
    assertThat(user2Cookie, nullValue());

    // create user 1
    ResponseEntity<UserDto> response = template.getForEntity(createURL("/user"), UserDto.class);
    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    assertThat(response.getBody().getId(), notNullValue());
    user1Cookie = getCookies(response);
    assertThat(user1Cookie.size(), equalTo(1));
    user1 = response.getBody();
  }
}
