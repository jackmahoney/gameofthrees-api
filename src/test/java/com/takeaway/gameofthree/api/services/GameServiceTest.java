package com.takeaway.gameofthree.api.services;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.takeaway.gameofthree.api.config.GameConfig;
import com.takeaway.gameofthree.api.entities.Game;
import com.takeaway.gameofthree.api.entities.User;
import com.takeaway.gameofthree.api.exceptions.ResourceNotFoundException;
import com.takeaway.gameofthree.api.repositories.GameRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {

  @Captor ArgumentCaptor<Game> captor;

  @Mock RandomNumberService randomNumberService;

  @Mock GameConfig gameConfig;

  @Mock GameRepository gameRepository;

  @Mock MoveService moveService;

  @InjectMocks GameService gameService;

  @Mock User ownUser;

  @Before
  public void before() {
    when(ownUser.getId()).thenReturn(UUID.randomUUID());
    // random numbers for testing, not the real rules as we are mocking a config
    when(gameConfig.getMinStartingValue()).thenReturn(3);
    when(gameConfig.getMaxStartingValue()).thenReturn(5);
  }

  private User getUser() {
    User user = new User();
    user.setId(UUID.randomUUID());
    return user;
  }

  private Optional<User> getUserOpt() {
    return Optional.of(getUser());
  }

  private Game getGame(Optional<User> player1, Optional<User> player2) {
    Game game = new Game();
    game.setId(UUID.randomUUID());
    player1.ifPresent(u -> game.setPlayer1(u));
    player2.ifPresent(u -> game.setPlayer2(u));
    return game;
  }

  @Test
  public void returnsEmptyWhenNoPlayer1GamesForOwnUser() {
    List<Game> allGames = Arrays.asList(getGame(getUserOpt(), Optional.empty()));
    when(gameRepository.findAll()).thenReturn(allGames);
    List<Game> results = gameService.getGamesForUser(ownUser);
    assertThat(results.size(), equalTo(0));
  }

  @Test
  public void returnsGamesWhenAPlayer1or2GameForOwnUser() {
    List<Game> allGames =
        Arrays.asList(
            // cannot access this game
            getGame(getUserOpt(), Optional.empty()),
            // can access these games
            getGame(getUserOpt(), Optional.of(ownUser)),
            getGame(Optional.of(ownUser), Optional.empty()),
            getGame(Optional.of(ownUser), getUserOpt()));
    when(gameRepository.findAll()).thenReturn(allGames);
    List<Game> results = gameService.getGamesForUser(ownUser);
    assertThat(results.size(), equalTo(3));
  }

  @Test
  public void canGetGameById() throws ResourceNotFoundException {
    Game game = mock(Game.class);
    UUID id = UUID.randomUUID();
    when(gameRepository.findById(id)).thenReturn(Optional.of(game));
    assertThat(gameService.getGameById(id), equalTo(game));
  }

  @Test
  public void getByIdThrowsIfNotFound() throws ResourceNotFoundException {
    UUID id = UUID.randomUUID();
    when(gameRepository.findById(id)).thenReturn(Optional.empty());
    assertThatThrownBy(() -> gameService.getGameById(id))
        .isInstanceOf(ResourceNotFoundException.class);
  }

  @Test
  public void canCreateGameWithCorrectInitialConfig() {
    User user = mock(User.class);
    when(randomNumberService.getRandomIntegerBetween(
            gameConfig.getMinStartingValue(), gameConfig.getMaxStartingValue()))
        .thenReturn(5);
    gameService.createGame(user, Optional.empty());
    verify(gameRepository, times(1)).save(captor.capture());
    Game game = captor.getValue();
    assertThat(game.getStartingValue(), equalTo(5));
    assertThat(game.getPlayer1(), equalTo(user));
    assertThat(game.getCurrentValue(), equalTo(5));
    assertThat(game.getWinner(), equalTo(Optional.empty()));
    assertThat(game.getCurrentPlayer(), equalTo(user));
    assertThat(game.getPlayer2(), equalTo(Optional.empty()));
  }
}
