package com.takeaway.gameofthree.api.services;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import com.takeaway.gameofthree.api.entities.User;
import com.takeaway.gameofthree.api.repositories.UserRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserServiceTest {

  @Captor ArgumentCaptor<User> captor;

  @Mock UserRepository userRepository;

  @InjectMocks UserService userService;

  final String sessionId = "test-session-id";

  @Mock HttpSession session;

  @Before
  public void before() {
    when(session.getId()).thenReturn(sessionId);
  }

  @Test
  public void canCreateNewUserForSession() {
    when(userRepository.findBySessionId(sessionId)).thenReturn(Optional.empty());
    userService.getUserForSession(session);

    verify(userRepository, times(1)).save(captor.capture());
    assertThat(captor.getValue().getSessionId(), equalTo(sessionId));
  }

  @Test
  public void canGetById() {
    UUID id = UUID.randomUUID();
    userService.getUserByID(id);
    verify(userRepository, times(1)).findById(id);
  }

  @Test
  public void canGetAll() {
    userService.getUsers();
    verify(userRepository, times(1)).findAll();
  }

  private User getUser() {
    User user = new User();
    user.setId(UUID.randomUUID());
    return user;
  }

  @Test
  public void canGetFriends() {
    User ownUser = getUser();
    List<User> all = Arrays.asList(ownUser, getUser(), getUser());
    when(userRepository.findAll()).thenReturn(all);
    List<User> friends = userService.getFriends(ownUser);
    assertThat(friends.size(), equalTo(2));
    assertThat(friends.get(0).getId().equals(ownUser.getId()), equalTo(false));
    assertThat(friends.get(1).getId().equals(ownUser.getId()), equalTo(false));
  }
}
