package com.takeaway.gameofthree.api.services;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.takeaway.gameofthree.api.config.GameConfig;
import com.takeaway.gameofthree.api.entities.Game;
import com.takeaway.gameofthree.api.entities.Move;
import com.takeaway.gameofthree.api.entities.User;
import com.takeaway.gameofthree.api.enums.MoveAction;
import com.takeaway.gameofthree.api.repositories.MoveRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MoveServiceTest {

  @Captor ArgumentCaptor<Move> moveCaptor;

  @Mock GameConfig gameConfig;

  @Mock MoveRepository moveRepository;

  @InjectMocks MoveService moveService;

  @Mock User user;

  @Mock Game game;

  @Before
  public void before() {
    when(gameConfig.getDivisor()).thenReturn(3);
  }

  @Test
  public void canPass() {
    when(game.getCurrentValue()).thenReturn(5);
    moveService.createMove(game, user, MoveAction.PASS);
    verify(moveRepository, times(1)).save(moveCaptor.capture());

    Move move = moveCaptor.getValue();
    assertThat(move.getInput(), equalTo(game.getCurrentValue()));
    assertThat(move.getPlayer(), equalTo(user));
    assertThat(move.getAction(), equalTo(MoveAction.PASS));
    assertThat(move.getGame(), equalTo(game));
    assertThat(move.getOutput(), equalTo(5 / 3));
  }

  @Test
  public void canAdd() {
    when(game.getCurrentValue()).thenReturn(5);
    moveService.createMove(game, user, MoveAction.ADD_1);
    verify(moveRepository, times(1)).save(moveCaptor.capture());

    Move move = moveCaptor.getValue();
    assertThat(move.getAction(), equalTo(MoveAction.ADD_1));
    assertThat(move.getOutput(), equalTo(6 / 3));
  }

  @Test
  public void canSubtract() {
    when(game.getCurrentValue()).thenReturn(5);
    moveService.createMove(game, user, MoveAction.SUBTRACT_1);
    verify(moveRepository, times(1)).save(moveCaptor.capture());

    Move move = moveCaptor.getValue();
    assertThat(move.getAction(), equalTo(MoveAction.SUBTRACT_1));
    assertThat(move.getOutput(), equalTo(4 / 3));
  }
}
