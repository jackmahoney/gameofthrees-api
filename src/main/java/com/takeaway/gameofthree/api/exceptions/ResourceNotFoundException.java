package com.takeaway.gameofthree.api.exceptions;

public class ResourceNotFoundException extends Throwable {
  public ResourceNotFoundException() {}

  public ResourceNotFoundException(String s) {
    super(s);
  }
}
