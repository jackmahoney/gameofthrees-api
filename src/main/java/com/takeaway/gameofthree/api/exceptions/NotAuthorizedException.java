package com.takeaway.gameofthree.api.exceptions;

public class NotAuthorizedException extends Throwable {
  public NotAuthorizedException() {}

  public NotAuthorizedException(String s) {
    super(s);
  }
}
