package com.takeaway.gameofthree.api.exceptions;

public class BadRequestException extends Throwable {
  public BadRequestException() {}

  public BadRequestException(String s) {
    super(s);
  }
}
