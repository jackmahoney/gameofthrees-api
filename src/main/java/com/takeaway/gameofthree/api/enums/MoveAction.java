package com.takeaway.gameofthree.api.enums;

public enum MoveAction {
  SUBTRACT_1,
  PASS,
  ADD_1
}
