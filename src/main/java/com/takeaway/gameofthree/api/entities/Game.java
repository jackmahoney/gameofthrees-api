package com.takeaway.gameofthree.api.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.*;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Table(name = "games")
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class Game extends UUIDEntity {

  Integer startingValue;

  Integer currentValue;

  @ManyToOne(
    fetch = FetchType.LAZY,
    cascade = {CascadeType.MERGE, CascadeType.PERSIST}
  )
  @JoinColumn(name = "player_1_id")
  User player1;

  @ManyToOne(
    fetch = FetchType.LAZY,
    cascade = {CascadeType.MERGE, CascadeType.PERSIST}
  )
  @JoinColumn(name = "player_2_id")
  User player2;

  @ManyToOne(
    fetch = FetchType.LAZY,
    cascade = {CascadeType.MERGE, CascadeType.PERSIST}
  )
  @JoinColumn(name = "winner_id")
  User winner;

  @ManyToOne(
    fetch = FetchType.LAZY,
    cascade = {CascadeType.MERGE, CascadeType.PERSIST}
  )
  @JoinColumn(name = "current_player_id")
  User currentPlayer;

  public Optional<User> getWinner() {
    return Optional.ofNullable(winner);
  }

  // optional as a game can consist of one player playing against a computer
  public Optional<User> getPlayer2() {
    return Optional.ofNullable(player2);
  }

  @OneToMany(
    targetEntity = Move.class,
    mappedBy = "game",
    fetch = FetchType.LAZY,
    cascade = CascadeType.ALL,
    orphanRemoval = true
  )
  List<Move> moves = new ArrayList<>();
}
