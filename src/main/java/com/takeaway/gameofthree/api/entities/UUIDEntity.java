package com.takeaway.gameofthree.api.entities;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@Getter
@Setter
@MappedSuperclass
public class UUIDEntity {

  /**
   * Use a UUID for all entities for security reasons. Incremental user ids give away business
   * information and make scanning our user information easier for bad actors.
   */
  @Id
  @Column(name = "id", columnDefinition = "UUID")
  @GeneratedValue(generator = "UUID")
  @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
  UUID id;

  @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
  LocalDateTime created;

  @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
  LocalDateTime updated;

  @PreUpdate
  void onUpdate() {
    updated = LocalDateTime.now();
  }

  @PrePersist
  void onCreate() {
    updated = LocalDateTime.now();
    created = LocalDateTime.now();
  }
}
