package com.takeaway.gameofthree.api.entities;

import javax.persistence.*;
import javax.persistence.Column;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Table(name = "users")
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class User extends UUIDEntity {

  @Column(unique = true)
  String sessionId;
}
