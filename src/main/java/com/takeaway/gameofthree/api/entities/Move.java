package com.takeaway.gameofthree.api.entities;

import javax.persistence.*;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import com.takeaway.gameofthree.api.enums.MoveAction;

@Table(name = "moves")
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class Move extends UUIDEntity {

  @ManyToOne(
    fetch = FetchType.LAZY,
    cascade = {CascadeType.MERGE, CascadeType.PERSIST}
  )
  @JoinColumn(name = "game_id")
  Game game;

  @ManyToOne(
    fetch = FetchType.LAZY,
    cascade = {CascadeType.MERGE, CascadeType.PERSIST}
  )
  @JoinColumn(name = "player_id")
  User player;

  // what was the number the user started with
  Integer input;

  // what action did user perform in move
  @Column(name = "move_action")
  @Enumerated(EnumType.STRING)
  MoveAction action;

  // what was the end result of the users move
  Integer output;
}
