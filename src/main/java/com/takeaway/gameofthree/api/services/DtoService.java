package com.takeaway.gameofthree.api.services;

import java.util.stream.Collectors;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import com.takeaway.gameofthree.api.dtos.GameDto;
import com.takeaway.gameofthree.api.dtos.MoveDto;
import com.takeaway.gameofthree.api.dtos.UserDto;
import com.takeaway.gameofthree.api.entities.Game;
import com.takeaway.gameofthree.api.entities.Move;
import com.takeaway.gameofthree.api.entities.User;

import org.springframework.stereotype.Service;

/** Service for converting entities into public facing DTOs */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DtoService {

  public UserDto convert(User user) {
    return UserDto.builder()
        .id(user.getId())
        .updated(user.getUpdated())
        .created(user.getCreated())
        .build();
  }

  public GameDto convert(Game game) {
    return GameDto.builder()
        .id(game.getId())
        .updated(game.getUpdated())
        .created(game.getCreated())
        .startingValue(game.getStartingValue())
        .currentValue(game.getCurrentValue())
        .player1(game.getPlayer1().getId())
        .player2(game.getPlayer2().map(User::getId))
        .winner(game.getWinner().map(User::getId))
        .currentPlayer(game.getCurrentPlayer().getId())
        .moves(game.getMoves().stream().map(this::convert).collect(Collectors.toList()))
        .build();
  }

  private MoveDto convert(Move move) {
    return MoveDto.builder()
        .id(move.getId())
        .updated(move.getUpdated())
        .created(move.getCreated())
        .action(move.getAction())
        .input(move.getInput())
        .output(move.getOutput())
        .player(move.getPlayer().getId())
        .build();
  }
}
