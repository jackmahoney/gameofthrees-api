package com.takeaway.gameofthree.api.services;

import java.util.Random;

import org.springframework.stereotype.Service;

@Service
public class RandomNumberService {
  public Integer getRandomIntegerBetween(Integer min, Integer max) {
    Random r = new Random();
    return r.nextInt((max - min) + 1) + min;
  }
}
