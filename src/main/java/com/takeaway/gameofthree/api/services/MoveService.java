package com.takeaway.gameofthree.api.services;

import javax.transaction.Transactional;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import com.takeaway.gameofthree.api.config.GameConfig;
import com.takeaway.gameofthree.api.entities.Game;
import com.takeaway.gameofthree.api.entities.Move;
import com.takeaway.gameofthree.api.entities.User;
import com.takeaway.gameofthree.api.enums.MoveAction;
import com.takeaway.gameofthree.api.repositories.MoveRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Move service that implements the game logic and rules around moving */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MoveService {

  @Autowired GameConfig gameConfig;

  @Autowired MoveRepository moveRepository;

  /** Make a new move entity and persist */
  @Transactional
  public Move createMove(Game game, User user, MoveAction moveAction) {
    Move move = new Move();
    move.setGame(game);
    move.setPlayer(user);
    move.setAction(moveAction);
    move.setInput(game.getCurrentValue());
    move.setOutput(applyActionAndDivide(game.getCurrentValue(), moveAction));
    return moveRepository.save(move);
  }

  /** A move action increments the input value */
  private Integer incrementInputForAction(Integer input, MoveAction moveAction) {
    switch (moveAction) {
      case PASS:
        return input;
      case ADD_1:
        return input + 1;
      case SUBTRACT_1:
        return input - 1;
      default:
        throw new RuntimeException("Unknown move action type");
    }
  }

  /** Each move increments the input then divides by the divisor */
  private Integer applyActionAndDivide(Integer input, MoveAction moveAction) {
    return incrementInputForAction(input, moveAction) / gameConfig.getDivisor();
  }
}
