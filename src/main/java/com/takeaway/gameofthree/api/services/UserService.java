package com.takeaway.gameofthree.api.services;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import com.takeaway.gameofthree.api.entities.User;
import com.takeaway.gameofthree.api.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserService {

  @Autowired UserRepository userRepository;

  /**
   * Create or get user for given session NOTE in this demo parallel calls could cause issues here
   * with createIfNotExists but for purposes of this assignment parallelism will not factor. In
   * production it's a different story
   */
  @Transactional
  public User getUserForSession(HttpSession session) {
    Optional<User> userResult = userRepository.findBySessionId(session.getId());
    if (userResult.isPresent()) {
      return userResult.get();
    } else {
      User user = new User();
      user.setSessionId(session.getId());
      return userRepository.save(user);
    }
  }

  public Optional<User> getUserByID(UUID uuid) {
    return userRepository.findById(uuid);
  }

  /** Get all users */
  public List<User> getUsers() {
    return userRepository.findAll();
  }

  /** Get all users that are not given user */
  public List<User> getFriends(User user) {
    return getUsers()
        .stream()
        .filter(u -> !u.getId().equals(user.getId()))
        .collect(Collectors.toList());
  }
}
