package com.takeaway.gameofthree.api.services;

import java.util.*;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import com.takeaway.gameofthree.api.config.GameConfig;
import com.takeaway.gameofthree.api.entities.Game;
import com.takeaway.gameofthree.api.entities.Move;
import com.takeaway.gameofthree.api.entities.User;
import com.takeaway.gameofthree.api.enums.MoveAction;
import com.takeaway.gameofthree.api.exceptions.BadRequestException;
import com.takeaway.gameofthree.api.exceptions.NotAuthorizedException;
import com.takeaway.gameofthree.api.exceptions.ResourceNotFoundException;
import com.takeaway.gameofthree.api.repositories.GameRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Game service for game CRUD and child (move, user) association */
@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GameService {

  @Autowired GameConfig gameConfig;

  @Autowired MoveService moveService;

  @Autowired GameRepository gameRepository;

  @Autowired RandomNumberService randomNumberService;

  /** Check whether user can access a game This means user is either player 1 or player 2 */
  private boolean canUserAccessGame(Game g, User user) {
    // as user can access a game if they are player one or two
    return g.getPlayer1().getId().equals(user.getId())
        || g.getPlayer2().isPresent() && g.getPlayer2().get().getId().equals(user.getId());
  }

  /**
   * Get games that a user can access
   *
   * @see this.canUserAccessGame
   */
  public List<Game> getGamesForUser(User user) {
    return gameRepository
        .findAll()
        .stream()
        // return games that have the user as first or second player
        // this is inefficient at scale but sufficient for demo purposes
        .filter(g -> canUserAccessGame(g, user))
        .collect(Collectors.toList());
  }

  /** Get game by id or throw not found */
  public Game getGameById(UUID id) throws ResourceNotFoundException {
    return gameRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
  }

  /** Get game by id and verify user can access it */
  public Game getGameByIdForUser(UUID id, User user)
      throws ResourceNotFoundException, NotAuthorizedException {
    Game game = getGameById(id);
    if (canUserAccessGame(game, user)) {
      return game;
    } else {
      throw new NotAuthorizedException();
    }
  }

  /** Create a new game for a given user with an optional second user */
  @Transactional
  public Game createGame(User user, Optional<User> player2) {
    Game game = new Game();
    game.setStartingValue(
        randomNumberService.getRandomIntegerBetween(
            gameConfig.getMinStartingValue(), gameConfig.getMaxStartingValue()));
    game.setPlayer1(user);
    game.setCurrentValue(game.getStartingValue());
    game.setCurrentPlayer(user);
    player2.ifPresent(game::setPlayer2);
    return gameRepository.save(game);
  }

  /** Attempt to add a user to a given game */
  @Transactional
  public Game joinGame(UUID id, User user) throws ResourceNotFoundException, BadRequestException {
    Game game = getGameById(id);
    if (game.getPlayer1().getId().equals(user.getId())) {
      throw new BadRequestException("Cannot join game you started");
    } else if (game.getPlayer2().isPresent()) {
      throw new BadRequestException("Cannot join a game when player 2 is already present");
    } else {
      game.setPlayer2(user);
      // if player 2 is joining after player 1 has already made a move then
      // make player 2 the current player
      if (game.getCurrentPlayer().getId().equals(game.getPlayer1().getId())
          && game.getMoves().size() > 0) {
        game.setCurrentPlayer(user);
      }
      return gameRepository.save(game);
    }
  }

  /**
   * Attempt a move on the given game for the given user with the given move action Apply the result
   * to the game and determine if a win has occurred
   */
  @Transactional
  public Game attemptMove(Game game, User user, MoveAction moveAction) throws BadRequestException {
    User currentPlayer = game.getCurrentPlayer();
    if (game.getWinner().isPresent()) {
      throw new BadRequestException("Game already won by " + game.getWinner().get().getId());
    } else if (!game.getPlayer2().isPresent() && game.getMoves().size() > 0) {
      throw new BadRequestException("You cannot play more than one move when alone");
    } else if (currentPlayer.getId().equals(user.getId())) {
      Move move = moveService.createMove(game, user, moveAction);
      game.getMoves().add(move);
      game.setCurrentPlayer(getNextPlayer(game));
      game.setCurrentValue(move.getOutput());
      if (move.getOutput().equals(gameConfig.getWinningValue())) {
        game.setWinner(currentPlayer);
      }
      return gameRepository.save(game);
    } else {
      throw new BadRequestException("Player attempting move is not current game player");
    }
  }

  /**
   * Determine which player has the right to make a move Typically the player who did not make the
   * last move or player 1 if only one player
   */
  private User getNextPlayer(Game game) {
    if (game.getPlayer2().isPresent()) {
      return game.getCurrentPlayer().getId().equals(game.getPlayer1().getId())
          ? game.getPlayer2().get()
          : game.getPlayer1();
    } else {
      return game.getPlayer1();
    }
  }
}
