package com.takeaway.gameofthree.api.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.takeaway.gameofthree.api.entities.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface UserRepository extends CrudRepository<User, UUID> {
  Optional<User> findBySessionId(String sessionId);

  List<User> findAll();
}
