package com.takeaway.gameofthree.api.repositories;

import java.util.UUID;

import com.takeaway.gameofthree.api.entities.Move;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface MoveRepository extends CrudRepository<Move, UUID> {}
