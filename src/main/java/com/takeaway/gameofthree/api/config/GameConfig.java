package com.takeaway.gameofthree.api.config;

import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "game")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GameConfig {
  @NotNull Integer winningValue;
  @NotNull Integer minStartingValue;
  @NotNull Integer maxStartingValue;
  @NotNull Integer divisor;
}
