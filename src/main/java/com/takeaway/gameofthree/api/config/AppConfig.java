package com.takeaway.gameofthree.api.config;

import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app")
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AppConfig {
  @NotNull String brandName;
}
