package com.takeaway.gameofthree.api.config;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import com.google.common.base.Predicates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@FieldDefaults(level = AccessLevel.PRIVATE)
@Profile("!test")
public class SwaggerConfig {

  @Autowired AppConfig appConfig;

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        // fix LocalDate(Time) sdk issues
        .directModelSubstitute(LocalDate.class, String.class)
        .directModelSubstitute(LocalDateTime.class, String.class)
        .select()
        .apis(RequestHandlerSelectors.any())
        .paths(PathSelectors.any())
        .paths(Predicates.not(PathSelectors.regex("/error")))
        .paths(Predicates.not(PathSelectors.regex("/")))
        .build()
        .apiInfo(apiInfo());
  }

  private static final String DOC_EXPANSION = "list"; // none, full

  @Bean
  public UiConfiguration uiConfig() {
    return new UiConfiguration(
        null,
        DOC_EXPANSION,
        "alpha",
        "schema",
        UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS,
        true,
        false,
        null);
  }

  private ApiInfo apiInfo() {
    return new ApiInfo(
        appConfig.getBrandName() + " API Documentation",
        String.format(
            "Swagger documentation and interactive UI for the %s API. Please see the README for instructions before playing.",
            appConfig.getBrandName()),
        "0.0.1",
        "",
        null,
        "",
        "",
        Collections.emptyList());
  }
}
