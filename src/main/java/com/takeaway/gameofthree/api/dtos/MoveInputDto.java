package com.takeaway.gameofthree.api.dtos;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.takeaway.gameofthree.api.enums.MoveAction;

import io.swagger.annotations.ApiModelProperty;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MoveInputDto {
  @ApiModelProperty("Action that player wishes to make in move")
  @NotNull
  MoveAction action;
}
