package com.takeaway.gameofthree.api.dtos;

import java.util.Optional;
import java.util.UUID;

import lombok.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

/** Options for creating or updating a game */
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GameCreateDto {
  @Builder.Default
  @ApiModelProperty(value = "Optional second player for this game")
  private UUID optionalPlayer2 = null;

  @JsonIgnore
  public Optional<UUID> getPlayer2() {
    return Optional.ofNullable(optionalPlayer2);
  }

  @JsonIgnore
  public void setPlayer2(Optional<UUID> uuid) {
    optionalPlayer2 = uuid.orElse(null);
  }
}
