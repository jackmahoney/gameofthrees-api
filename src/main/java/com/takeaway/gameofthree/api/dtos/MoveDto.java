package com.takeaway.gameofthree.api.dtos;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import com.takeaway.gameofthree.api.enums.MoveAction;

import io.swagger.annotations.ApiModelProperty;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MoveDto {

  @ApiModelProperty("ID")
  @NotNull
  UUID id;

  @ApiModelProperty("Created at")
  @NotNull
  LocalDateTime created;

  @ApiModelProperty("Updated at")
  @NotNull
  LocalDateTime updated;

  @ApiModelProperty("Player who made the move")
  @NotNull
  UUID player;

  @ApiModelProperty("Game value before move was made")
  @NotNull
  Integer input;

  @ApiModelProperty("Game value after the move was made")
  @NotNull
  Integer output;

  @ApiModelProperty("Action that player made in move")
  @NotNull
  MoveAction action;
}
