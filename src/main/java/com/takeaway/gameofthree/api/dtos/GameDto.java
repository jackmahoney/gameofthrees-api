package com.takeaway.gameofthree.api.dtos;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModelProperty;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GameDto {

  @ApiModelProperty("ID")
  @NotNull
  UUID id;

  @ApiModelProperty("Created at")
  @NotNull
  LocalDateTime created;

  @ApiModelProperty("Updated at")
  @NotNull
  LocalDateTime updated;

  Integer startingValue;

  Integer currentValue;

  UUID player1;

  Optional<UUID> player2;

  Optional<UUID> winner;

  UUID currentPlayer;

  @ApiModelProperty("Moves in game")
  @NotNull
  List<MoveDto> moves;
}
