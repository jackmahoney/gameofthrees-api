package com.takeaway.gameofthree.api.dtos;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModelProperty;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

  @ApiModelProperty("ID")
  @NotNull
  UUID id;

  @ApiModelProperty("Created at")
  @NotNull
  LocalDateTime created;

  @ApiModelProperty("Updated at")
  @NotNull
  LocalDateTime updated;
}
