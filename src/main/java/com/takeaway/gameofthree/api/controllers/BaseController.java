package com.takeaway.gameofthree.api.controllers;

import lombok.extern.slf4j.Slf4j;

import com.takeaway.gameofthree.api.dtos.ErrorResponseDto;
import com.takeaway.gameofthree.api.exceptions.BadRequestException;
import com.takeaway.gameofthree.api.exceptions.NotAuthorizedException;
import com.takeaway.gameofthree.api.exceptions.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
public class BaseController {

  @Autowired ExceptionAdvisor exceptionAdvisor;

  @ExceptionHandler(ResourceNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ResponseBody
  public ErrorResponseDto handleResourceNotFoundException(ResourceNotFoundException exception) {
    return exceptionAdvisor.getErrorResponseDto(exception, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(NotAuthorizedException.class)
  @ResponseStatus(HttpStatus.UNAUTHORIZED)
  @ResponseBody
  public ErrorResponseDto handleNotAuthorizedException(NotAuthorizedException exception) {
    return exceptionAdvisor.getErrorResponseDto(exception, HttpStatus.UNAUTHORIZED);
  }

  @ExceptionHandler(BadRequestException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorResponseDto handleBadRequestException(BadRequestException exception) {
    return exceptionAdvisor.getErrorResponseDto(exception, HttpStatus.BAD_REQUEST);
  }
}
