package com.takeaway.gameofthree.api.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class IndexController extends BaseController {

  @GetMapping("/")
  public RedirectView index() {
    return new RedirectView("/swagger-ui.html");
  }
}
