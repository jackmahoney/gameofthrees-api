package com.takeaway.gameofthree.api.controllers;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.takeaway.gameofthree.api.dtos.UserDto;
import com.takeaway.gameofthree.api.entities.User;
import com.takeaway.gameofthree.api.services.DtoService;
import com.takeaway.gameofthree.api.services.UserService;

import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class FriendsController extends BaseController {

  @Autowired DtoService dtoService;

  @Autowired UserService userService;

  @ApiOperation(
    value = "List all other users",
    notes = "List all users other than your own. Useful for playing against friends"
  )
  @GetMapping("/friends")
  @Transactional
  public List<UserDto> getFriends(HttpSession session) throws IOException {
    User user = userService.getUserForSession(session);
    return userService
        .getFriends(user)
        .stream()
        .map(dtoService::convert)
        .collect(Collectors.toList());
  }
}
