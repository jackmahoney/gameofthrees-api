package com.takeaway.gameofthree.api.controllers;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;

import lombok.extern.slf4j.Slf4j;

import com.takeaway.gameofthree.api.dtos.GameCreateDto;
import com.takeaway.gameofthree.api.dtos.GameDto;
import com.takeaway.gameofthree.api.dtos.MoveInputDto;
import com.takeaway.gameofthree.api.entities.Game;
import com.takeaway.gameofthree.api.entities.User;
import com.takeaway.gameofthree.api.exceptions.BadRequestException;
import com.takeaway.gameofthree.api.exceptions.NotAuthorizedException;
import com.takeaway.gameofthree.api.exceptions.ResourceNotFoundException;
import com.takeaway.gameofthree.api.services.DtoService;
import com.takeaway.gameofthree.api.services.GameService;
import com.takeaway.gameofthree.api.services.UserService;

import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
public class GameController extends BaseController {

  @Autowired DtoService dtoService;

  @Autowired UserService userService;

  @Autowired GameService gameService;

  @ApiOperation(value = "List own user games", notes = "List games associated with user")
  @GetMapping("/games")
  @Transactional
  public List<GameDto> getGamesForUser(HttpSession session) {
    return gameService
        .getGamesForUser(userService.getUserForSession(session))
        .stream()
        .map(dtoService::convert)
        .collect(Collectors.toList());
  }

  @ApiOperation(value = "Create a game", notes = "Create a game with optional player 2")
  @PostMapping("/games")
  @Transactional
  @ResponseStatus(HttpStatus.CREATED)
  public GameDto createGame(HttpSession session, @Valid @RequestBody GameCreateDto gameCreateDto) {
    User user = userService.getUserForSession(session);
    Optional<User> player2 = gameCreateDto.getPlayer2().flatMap(userService::getUserByID);
    return dtoService.convert(gameService.createGame(user, player2));
  }

  @ApiOperation(value = "Get a game")
  @GetMapping("/games/{uuid}")
  @Transactional
  public GameDto getGame(HttpSession session, @PathVariable("uuid") UUID id)
      throws ResourceNotFoundException, NotAuthorizedException {
    User user = userService.getUserForSession(session);
    Game game = gameService.getGameByIdForUser(id, user);
    return dtoService.convert(game);
  }

  @ApiOperation(value = "Join a game")
  @PostMapping("/games/{uuid}/join")
  @Transactional
  public GameDto joinGame(HttpSession session, @PathVariable("uuid") UUID id)
      throws ResourceNotFoundException, BadRequestException {
    User user = userService.getUserForSession(session);
    Game game = gameService.joinGame(id, user);
    return dtoService.convert(game);
  }

  @ApiOperation(value = "Make a move in a game")
  @PostMapping("/games/{uuid}/move")
  @Transactional
  public GameDto updateGame(
      HttpSession session,
      @PathVariable("uuid") UUID id,
      @Valid @RequestBody MoveInputDto moveInputDto)
      throws ResourceNotFoundException, NotAuthorizedException, BadRequestException {
    User user = userService.getUserForSession(session);
    Game game = gameService.getGameByIdForUser(id, user);
    Game updatedGame = gameService.attemptMove(game, user, moveInputDto.getAction());
    return dtoService.convert(updatedGame);
  }
}
