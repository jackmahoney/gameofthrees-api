package com.takeaway.gameofthree.api.controllers;

import lombok.extern.slf4j.Slf4j;

import com.takeaway.gameofthree.api.dtos.ErrorResponseDto;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ExceptionAdvisor {

  public ErrorResponseDto getErrorResponseDto(Throwable e, HttpStatus status) {
    log.error(e.getClass().getSimpleName() + " exception occurred: " + e.getMessage());
    e.printStackTrace();
    return new ErrorResponseDto(e.getClass().getSimpleName(), e.getMessage(), status.value());
  }
}
