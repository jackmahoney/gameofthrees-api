package com.takeaway.gameofthree.api.controllers;

import java.io.IOException;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.takeaway.gameofthree.api.dtos.UserDto;
import com.takeaway.gameofthree.api.services.DtoService;
import com.takeaway.gameofthree.api.services.UserService;

import io.swagger.annotations.ApiOperation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class UserController extends BaseController {

  @Autowired DtoService dtoService;

  @Autowired UserService userService;

  @ApiOperation(
    value = "Get own user",
    notes =
        "A user is associated with every session. It will be created by this endpoint if it does not already exist"
  )
  @GetMapping("/user")
  @Transactional
  public UserDto getUser(HttpSession session) throws IOException {
    return dtoService.convert(userService.getUserForSession(session));
  }
}
